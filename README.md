# Toolforge alert rules

## How to use this repository

See https://wikitech.wikimedia.org/wiki/Alertmanager for the basic idea.
However, please there are a few differences.

### Filtering instances

By default these alerts are deployed to the Prometheus instances in the tools
and toolsbeta Cloud VPS projects. If you have a good reason to not deploy to one
of them (and ask a Prometheus expert if you're not sure, because it's likely you
don't), you can use a comment like `# deploy-tag: project-tools` in the alert
file for example.

### Organization

Because this repository is not used by multiple different teams, organizing
alerts by team would not make sense. For that reason it's organized by Toolforge
components.

### Testing

You'll need to install `promtool` (currently shipped with
[prometheus](https://github.com/prometheus/prometheus)) and
[`pint`](https://github.com/cloudflare/pint).

To do the full set of tests you can run:

> tox

If you just want to test a single prometheus test file, you can use `promtool`
directly, like:

> promtool test rules kubernetes/worker_out_of_space_test.yaml

If you want to match exactly what CI does, you have to run (will also check for
the runbooks existing):

> tox -e ALL
